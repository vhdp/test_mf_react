import React, {useState} from 'react';
import {ErrorBoundary} from './ErrorBoundary';


const RemoteDialog = React.lazy(() => import('dialog/Dialog'));

export function App() {


    return (
        <div>
            <h1 style={{fontSize: '64px'}}>Main App</h1>

            <h2>
                Imported &lt;Dialog /&gt; from another domain at runtime
            </h2>

            <ErrorBoundary>
                <React.Suspense fallback="...">
                    <RemoteDialog
                    />
                </React.Suspense>
            </ErrorBoundary>
        </div>
    );
}
