import React from 'react';
import './Container.scss'

export function Container() {
  return (
    <div>
      <h1>
        "Button APP" which exposes &lt;Button /&gt; component with styles, fonts &amp; images to
        other apps.
      </h1>
    </div>
  );
}
