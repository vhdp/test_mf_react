export const header = [
    {Header: "First Name", accessor: "firstName"},
    {Header: "Last Name", accessor: "lastName"},
    {Header: "Age", accessor: "age"},
    {Header: "Visits", accessor: "visits"},
    {Header: "Status", accessor: "status"},
    {Header: "Profile Progress", accessor: "progress"}
]


export const data = [
    {
        "firstName": "building",
        "lastName": "mountain",
        "age": 1,
        "visits": 50,
        "progress": 61,
        "status": "relationship"
    }, {
        "firstName": "sack",
        "lastName": "chairs",
        "age": 16,
        "visits": 4,
        "progress": 69,
        "status": "relationship"
    }, {
        "firstName": "warning",
        "lastName": "truth",
        "age": 13,
        "visits": 89,
        "progress": 84,
        "status": "single"
    }, {
        "firstName": "platform",
        "lastName": "payment",
        "age": 11,
        "visits": 86,
        "progress": 4,
        "status": "complicated"
    }, {
        "firstName": "building",
        "lastName": "inflation",
        "age": 11,
        "visits": 65,
        "progress": 22,
        "status": "relationship"
    }, {
        "firstName": "department",
        "lastName": "computer",
        "age": 22,
        "visits": 50,
        "progress": 9,
        "status": "single"
    }
]