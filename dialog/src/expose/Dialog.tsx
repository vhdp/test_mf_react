import React, {useState} from 'react';
import {ErrorBoundary} from '../ErrorBoundary';
import {header, data} from '../fixtures/randomData'
import './Dialog.scss'

const RemoteButton = React.lazy(() => import('button/Button'));
const RemoteTable = React.lazy(() => import('table/Table'));
export default function Dialog() {
    const [cnt, setCnt] = useState(0);
    const columns = React.useMemo(
        () => header,
        []
    )
    const tableData = React.useMemo(() =>data, [])
    return (
        <div>
            <ErrorBoundary>
                <React.Suspense fallback="...">
                    <RemoteTable
                        columns={columns}
                        data={tableData}
                    />
                </React.Suspense>
            </ErrorBoundary>
            <ErrorBoundary>
                <React.Suspense fallback="...">
                    <RemoteButton
                        text={`The button is working! ${cnt}`}
                        onClick={() => {
                            setCnt(cnt + 1);
                        }}
                    />
                </React.Suspense>
            </ErrorBoundary>

        </div>
    );
}