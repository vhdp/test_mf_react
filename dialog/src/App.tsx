import React from 'react';
import Dialog from './expose/Dialog';


export function App() {

    return (
        <div>
            <h1 style={{fontSize: '64px'}}>Dialog App</h1>
            <h2>Imported &lt;Table /&gt; from another domain at runtime</h2>
            <h2>Imported &lt;Button /&gt; from another domain at runtime</h2>
            <Dialog/>
        </div>
    );
}
