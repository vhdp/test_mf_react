import { createWebHistory, createRouter } from "vue-router";
import Home from "./components/pages/Home.vue";
import Login from "./components/pages/Login.vue";
import Profile from "./components/pages/Profile.vue";

const routes = [
  {
    path: "/",
    name: 'Base',
    component: Login,
  },
  {
    path: "/login",
    component: Login,
  },
  {
    path: "/profile",
    name: 'Login',
    component: Profile,
  },
  {
    path: "/home",
    name: 'Home',
    component: Home,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});


export default router;