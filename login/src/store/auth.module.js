import AuthService from '../services/auth.service';

const user = JSON.parse(localStorage.getItem('user'));
const initialState = user
  ? { status: { loggedIn: true, profileChosen: user.profileChosen }, user }
  : { status: { loggedIn: false,  profileChosen: false }, user: null };

export const auth = {
  namespaced: true,
  state: initialState,
  actions: {
    login({ commit }, user) {
      return AuthService.login(user).then(
        user => {
          commit('loginSuccess', user);
          return Promise.resolve(user);
        },
        error => {
          commit('loginFailure');
          return Promise.reject(error);
        }
      );
    },
    profile({ commit }, profile) {
      return AuthService.profile(profile).then(
        profile => {
          commit('profileChosen', profile);
          return Promise.resolve(profile);
        },
        error => {
          commit('profileFailure');
          return Promise.reject(error);
        }
      );
    },
    logout({ commit }) {
      AuthService.logout();
      commit('logout');
    },
  },
  mutations: {
    loginSuccess(state, user) {
      state.status.loggedIn = true;
      state.user = user;
    },
    profileChosen(state, profile) {
      state.user.profileChosen = profile
      state.status.profileChosen = profile
    },
    loginFailure(state) {
      state.status.loggedIn = false;
      state.status.profileChosen = false;
      state.user = null;
    },
    logout(state) {
      state.status.loggedIn = false;
      state.status.profileChosen = false;
      state.user = null;
    },
  },
  getters: {
    getUser: state => {
      return state.user;
    },
    getUserStatus: state => {
      return state.status.loggedIn;
    },
    getUserProfile: state => {
      return state.status.profileChosen;
    }
  }
};
