import axios from 'axios';

const API_URL = 'http://10.33.57.49:8080/itcsWebClient/';


class AuthService {
  async loginReq() {
    const response = await axios(`${API_URL}login`)
    return response.status
  }

  async login(user) {
/*    const loginReq = await this.loginReq()
    console.log(loginReq);*/
    const result = {};
    return axios
      .post(API_URL + `login?`,
        {
          "action": "userLogin",
          "actionId": 1,
          "params": {"username": user.username, "password": user.password}
        },
        {
          params: {
            "username": user.username, "password": user.password
          },
          withCredentials: true
        }
      )
      .then(response => {
        if (response.data.hasOwnProperty('chooseProfile')) {
          result.userName = user.username
          result.profile = response.data.chooseProfile.values
          localStorage.setItem('user', JSON.stringify(result));
          return result
        } else {
          return response.data;
        }
      });
  }

  profile(profile) {
    return axios
      .post(API_URL + `login_action`,
        {
          "action": "chooseProfile",
          "actionId": 2,
          "params": {"profile": profile.profile}
        },
        {
          withCredentials: true
        }
      )
      .then(response => {
        if (response.status === 200) {
          const user = JSON.parse(localStorage.getItem('user'));
          user.profileChosen = profile.profile
          console.log(response);
          localStorage.setItem('user', JSON.stringify(user));
          return profile.profile;
        }
      });
  }

  logout() {
    localStorage.removeItem('user');
  }
}

export default new AuthService();