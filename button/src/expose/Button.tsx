import React from 'react';
import style from './Button.module.scss';

interface Button {
  text: string;
  onClick?: () => any;
}

export default function Button({ text, onClick }: Button) {
  return (
    <button className={style.button} onClick={onClick} title="MY_BTN">
      {text}
    </button>
  );
}
