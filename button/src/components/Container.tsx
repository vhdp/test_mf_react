import React from 'react';
import Button from '../expose/Button';
import './Container.scss'

export function Container() {
  return (
    <div>
      <h1>
        "Button APP" which exposes &lt;Button /&gt; component with styles, fonts &amp; images to
        other apps.
      </h1>
      <Button text="Simple Button" />
    </div>
  );
}
