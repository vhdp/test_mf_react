export const header = [
  {
    Header: 'Name',
    accessor: 'blend_name',
  },
  {
    Header: 'Origin',
    accessor: 'origin',
  },
  {
    Header: 'Sort',
    accessor: 'variety',
  },
  {
    Header: 'Notes',
    accessor: 'notes',
  },
  {
    Header: 'Intensity',
    accessor: 'intensifier',
  },
]


export const coffee =[
  {
    id: 3833,
    uid: "5a6965d8-4e57-4d7b-a862-7276a984a976",
    blend_name: "Captain's Delight",
    origin: "El Balsamo-Quetzaltepec, El Salvador",
    variety: "S795",
    notes: "dull, smooth, magnolia, dates, lavender",
    intensifier: "balanced"
  },
  {
    id: 7613,
    uid: "fcf96c52-0068-4d1f-beb7-f38f0c303a84",
    blend_name: "Spilt Java",
    origin: "Volcan, Panama",
    variety: "Blue Mountain",
    notes: "tart, juicy, tangerine, rose hips, cocoa powder",
    intensifier: "quick"
  },
  {
    id: 9866,
    uid: "afe136d6-3936-4505-9bef-c1194c140af8",
    blend_name: "Major Enlightenment",
    origin: "Kayanza, Burundi",
    variety: "Barbuk Sudan",
    notes: "complex, syrupy, lime, grassy, watermelon",
    intensifier: "balanced"
  },
  {
    id: 6802,
    uid: "56673fcb-9a02-41f0-a35c-208cbdbda472",
    blend_name: "The Bean",
    origin: "Northern Region, Arusha, Tanzania",
    variety: "Typica",
    notes: "quick, full, cola, granola, black-tea",
    intensifier: "pointed"
  },
  {
    id: 9706,
    uid: "cdab9d01-10d8-4fd3-bcb9-847ae286c0c1",
    blend_name: "Split Breaker",
    origin: "Kigeyo Washing Station, Rwanda",
    variety: "Typica",
    notes: "pointed, silky, almond, barley, molasses",
    intensifier: "dry"
  }
]