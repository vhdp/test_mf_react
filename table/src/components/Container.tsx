import React from 'react';
import Table from '../expose/Table';
import {header, coffee} from '../fixtures/randomData'
import './Container.scss'

export function Container() {
    const columns = React.useMemo(
        () => header,
        []
    )
    const data = React.useMemo(() =>coffee, [])

    return (
        <div>
            <h1>
                "Table APP" which exposes &lt;Table /&gt; component with styles, fonts &amp; images to
                other apps.
            </h1>
            <Table columns={columns} data={data}/>
        </div>
    );
}
